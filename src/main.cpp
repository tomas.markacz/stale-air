#include "Arduino.h"
#include "SoftwareSerial.h"
#include <MHZ19.h>
#include <TM1637Display.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <OneWire.h>
#include <DS18B20.h>

#define CYCLE_DURATION 500
#define DATA_ENDPOINT_URL "https://36lfj0slbj.execute-api.eu-west-1.amazonaws.com/dev/data"
const uint8_t DATA_ENDPOINT_SSL_FINGERPRINT[20] = {0x4D, 0x9F, 0x60, 0x78, 0x51, 0x84, 0x39, 0xF4, 0x1A, 0x14, 0x11, 0x38, 0xB0, 0xAC, 0x84, 0x65, 0x34, 0xB3, 0x0D, 0x0B};

#define WARM_UP_TIME 180000

#define LIGHT_LOW 0
#define LIGHT_MEDIUM 1
#define LIGHT_HIGH 2

#define BRIGHTNESS_LOW 0
#define BRIGHTNESS_MEDIUM 2
#define BRIGHTNESS_HIGH 7

#define BEEPER_PWM_DUTY 6
#define BEEPER_PWM_FREQUENCY 880

SoftwareSerial softwareSerial;
MHZ19 mhz19(&softwareSerial);
TM1637Display display(D5, D6);
WiFiClient wifiClient;
OneWire oneWire(D3);
DS18B20 ds18b20(&oneWire);

unsigned long cycle = 0;
int co2 = 0;
float temperature = 0;
uint8_t segments[4];
bool sendError = false;
int lightLevel = 2;

void setup() {
    Serial.begin(9600);
    softwareSerial.begin(9600, SWSERIAL_8N1, D7, D8, false, 256);
    ds18b20.begin();

    pinMode(D1, OUTPUT);
    pinMode(D2, OUTPUT);
    digitalWrite(D2, LOW);
    analogWriteFreq(BEEPER_PWM_FREQUENCY);

    delay(1000);

    mhz19.setAutoCalibration(false);
    ds18b20.requestTemperatures();

    Serial.printf("Connecting to Wi-Fi: %s\n", WIFI_SSID);
    WiFi.hostname("sensor");
    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
}

void loop() {
    if (cycle % 10 == 0) {
        MHZ19_RESULT response = mhz19.retrieveData();
        if (response != MHZ19_RESULT_OK) {
            co2 = 0;
            Serial.printf("MHZ19 error: %d\n", response);

        } else {
            co2 = mhz19.getCO2();
            Serial.printf("MHZ19: CO2 %dppm, temperature %d˚C\n", co2, mhz19.getTemperature());
        }

        if (ds18b20.isConversionComplete()) {
            temperature = ds18b20.getTempC();
            Serial.printf("DS18B20: temperature %.1f˚C\n", temperature);
            ds18b20.requestTemperatures();
        }
    }

    int analogReading = analogRead(A0);
    if (analogReading > 1000) {
        lightLevel = LIGHT_HIGH;

    } else if (analogReading > 750) {
        lightLevel = LIGHT_MEDIUM;

    } else {
        lightLevel = LIGHT_LOW;
    }

    if (co2 > 1000 && millis() >= WARM_UP_TIME && cycle % 2 == 1) {
        switch (lightLevel) {
            case LIGHT_HIGH:
                display.setBrightness(BRIGHTNESS_MEDIUM);
                break;

            case LIGHT_MEDIUM:
            case LIGHT_LOW:
                display.setBrightness(BRIGHTNESS_LOW);
                break;
        }

    } else {
        switch (lightLevel) {
            case LIGHT_HIGH:
                display.setBrightness(BRIGHTNESS_HIGH);
                break;

            case LIGHT_MEDIUM:
                display.setBrightness(BRIGHTNESS_MEDIUM);
                break;

            case LIGHT_LOW:
                display.setBrightness(BRIGHTNESS_LOW);
                break;
        }
    }

    if (co2 == 0 || millis() < WARM_UP_TIME) {
        segments[0] = SEG_G;
        segments[1] = SEG_G;
        segments[2] = SEG_G;
        segments[3] = SEG_G;

    } else {
        segments[0] = co2 >= 1000 ? display.encodeDigit(co2 / 1000) : 0;
        segments[1] = co2 >= 100 ? display.encodeDigit(co2 / 100 % 10) : 0;
        segments[2] = co2 >= 10 ? display.encodeDigit(co2 / 10 % 10) : 0;
        segments[3] = display.encodeDigit(co2 % 10);
    }

    if (WiFi.status() == WL_CONNECTED) {
        if (sendError && cycle % 2 == 0) {
            segments[1] |= SEG_DP;
        }

    } else {
        if (cycle % 4 == 0) {
            segments[0] |= SEG_DP;

        } else if (cycle % 2 == 1) {
            segments[1] |= SEG_DP;

        } else {
            segments[2] |= SEG_DP;
        }
    }

    display.setSegments(segments);

    if (co2 > 1500 && millis() >= WARM_UP_TIME && lightLevel > LIGHT_LOW
        && (cycle % 600 == 0 || cycle % 600 == 2 || cycle % 600 == 4)
    ) {
        analogWrite(D1, BEEPER_PWM_DUTY);

    } else {
        analogWrite(D1, 0);
    }

    if (cycle % 180 == 0 && WiFi.status() == WL_CONNECTED && co2 != 0 && millis() >= WARM_UP_TIME) {
        String data = String("{\"co2\":")
            + String(co2)
            + String(",\"temperature\":")
            + String(temperature, 1)
            + String("}");

        Serial.printf("Sending data: %s\n", data.c_str());

        HTTPClient httpClient;
        httpClient.begin(DATA_ENDPOINT_URL, DATA_ENDPOINT_SSL_FINGERPRINT);
        httpClient.addHeader("X-Api-Key", DATA_ENDPOINT_API_KEY);

        int statusCode = httpClient.POST(data);
        String body = httpClient.getString();
        httpClient.end();

        sendError = statusCode != 204;

        Serial.printf("Response status code: %d\n", statusCode);
        Serial.printf("Response body: %s\n", body.c_str());
    }

    delay(CYCLE_DURATION - millis() % CYCLE_DURATION);
    cycle++;
}
